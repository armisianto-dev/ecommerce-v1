import React from 'react';
import { Link } from 'react-router-dom';
import './BreadcrumbComponent.css';

const BreadcrumbComponent = (props) => {
    const lists = props.lists
    const category = props.category ? props.category : false;
    let lenLists = lists.length;
    return (
        <ol className={category ? 'breadcrumb breadcrumb-category mb-0' : 'breadcrumb mb-0'}>
            {lists.map((list, index) => {
                let classBreadcrumb = lenLists === (index + 1) ? 'breadcrumb-item active' : 'breadcrumb-item';
                let linkBreadcrumb = (lenLists === (index + 1) && category === false) ? list.title : <Link to={list.link}>{list.title}</Link>;
                return <li className={classBreadcrumb} key={index}>
                    {linkBreadcrumb}
                </li>
            })}
        </ol>
    );
}

export default BreadcrumbComponent;