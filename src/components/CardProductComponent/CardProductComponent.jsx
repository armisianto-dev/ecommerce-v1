import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './CardProductComponent.css';

const CardProductComponent = (props) => {
    let mobile = props.mobile;
    return (
        <Fragment>
            {props.title &&
                <div className="d-flex flex-row justify-content-between card-product-row-title mt-5 mb-3 px-3">
                    <h5>{props.title}</h5>
                    {!mobile &&
                        <a href="#">Lihat semua</a>
                    }
                </div>
            }
            <div className={mobile === true ? 'card-product-row card-product-mobile justify-content-start ' : 'card-product-row justify-content-start'}>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className={props.all ? 'card-product-wrapper all' : 'card-product-wrapper'}>
                    <Link to="/product/brand/title-product">
                        <div className={props.all ? 'card card-product-container all' : 'card card-product-container'}>
                            <img className="card-img-top" src="/no-image.png" alt="Card image cap" />
                            <div className="card-body p-2 d-flex flex-column justify-content-between">
                                <div>
                                    <span className="card-product-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</span>
                                    <span className="card-product-price">Rp 3.589.000</span>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                {mobile &&
                    <div className="card-product-wrapper d-flex align-items-center">
                        <a href="#" className="mx-auto text-secondary">
                            <i className="fa fa-arrow-alt-circle-right fa-3x"></i>
                        </a>
                    </div>
                }
            </div>
        </Fragment>
    );
}

export default CardProductComponent;