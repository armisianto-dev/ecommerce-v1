import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './CarouselComponent.css';

const CarouselComponent = (props) => {
    if (props.mobile === true) {
        return (
            <Carousel showArrows={false} showThumbs={false} showStatus={false} infiniteLoop autoPlay emulateTouch height="400px" className="carousel-mobile">
                <div>
                    <a href="#">
                        <img src="/slider/slide1-m.jpg" alt="slide1" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide2-m.jpg" alt="slide2" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide3-m.jpg" alt="slide3" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide4-m.jpg" alt="slide4" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide5-m.jpg" alt="slide5" />
                    </a>
                </div>
            </Carousel>
        );
    } else {
        return (
            <Carousel showArrows={false} showThumbs={false} showStatus={false} infiniteLoop autoPlay emulateTouch height="300px">
                <div>
                    <a href="#">
                        <img src="/slider/slide1.jpg" alt="slide1" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide2.jpg" alt="slide2" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide3.jpg" alt="slide3" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide4.jpg" alt="slide4" />
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="/slider/slide5.jpg" alt="slide5" />
                    </a>
                </div>
            </Carousel>
        );
    }

}

export default CarouselComponent;