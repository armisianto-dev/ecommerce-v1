import React, { Fragment, useEffect, useState } from 'react';
import { Button, Container, FormControl, InputGroup, ListGroup, Nav, Navbar } from "react-bootstrap";
import { Link } from 'react-router-dom';
import StringHelpers from '../../helpers/StringHelpers';
import './TopBarComponent.css';


const TopBarComponent = (props) => {
    const [scrolled, setScrolled] = useState();
    const [kategoriAktif, setKategoriAktif] = useState(0);
    let arr = Array.apply(null, { length: 15 }).map(Number.call, Number);
    let classTopBarMobile = scrolled ? 'd-flex justify-content-between align-items-center nav-mobile nav-mobile-onscroll px-2' : 'd-flex justify-content-between align-items-center nav-mobile px-2';

    useEffect(_ => {
        const handleScroll = _ => {
            if (window.pageYOffset > 270) {
                setScrolled(true)
            } else {
                setScrolled(false)
            }
        }
        window.addEventListener('scroll', handleScroll)
        return _ => {
            window.removeEventListener('scroll', handleScroll)
        }
    }, [])

    if (props.mobile === true) {
        return (
            <Fragment>
                <Navbar fixed="top" className={classTopBarMobile}>
                    <Nav>
                        <Nav.Link href="#" className="font-weight-light text-white">
                            <i className="fa fa-bars"></i>
                        </Nav.Link>
                    </Nav>
                    <Nav className="px-2 w-100">
                        <FormControl placeholder="Pencarian Produk" size="sm" />
                    </Nav>
                    <Nav>
                        <Nav.Link href="#" className="font-weight-light text-white">
                            <i className="fa fa-comments"></i>
                        </Nav.Link>
                        <Nav.Link href="#" className="user-topbar text-white">
                            <img src="/default-avatar.png" alt="user" className="img-user-topbar" />
                        </Nav.Link>
                    </Nav>
                </Navbar>
            </Fragment>
        );
    } else {
        return (
            <Fragment>
                <Navbar collapseOnSelect expand="md" variant="light" bg="white" fixed="top" className="px-0 py-3 py-sm-3 py-md-0 py-lg-0 d-flex flex-column">
                    <div className="w-100 d-flex justify-content-between align-items-center px-3">
                        <Navbar.Brand href="/">e-commerce</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse>
                            <Nav className="mr-auto">
                                <li className="nav-item dropdown">
                                    <a className="nav-link nav-link-icon dropdown-toggle font-weight-bold" href="#" onMouseEnter={() => setKategoriAktif(0)}>
                                        <span className="font-12">Kategori</span>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-kategori">
                                        <div className="arrow-up"></div>
                                        <div className="row px-1">
                                            <div className="col-3 col-kategori-group">

                                                <ListGroup variant="flush">
                                                    {arr.map((item, index) => {
                                                        return (<ListGroup.Item key={index} onMouseEnter={() => setKategoriAktif(item)} className={kategoriAktif === item ? 'active' : ''}>
                                                            <a href="#">Kategori {item + 1}</a>
                                                        </ListGroup.Item>)
                                                    })}
                                                </ListGroup>

                                            </div>
                                            <div className="col-9 col-kategori-list">
                                                {arr.map((item, index) => {
                                                    return (
                                                        <ul key={index} className={kategoriAktif === item ? 'display-block' : 'display-none'}>
                                                            <li>
                                                                <Link to={"/products/" + StringHelpers.string_to_slug('kategori ' + (item + 1)) + '/' + StringHelpers.string_to_slug('sub-kategori ' + (item + 1))}>Ini isi kategori {item + 1}</Link>
                                                            </li>
                                                        </ul>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <Nav.Link href="#" className="font-weight-light nav-link-icon relative">
                                    <i className="fa fa-shopping-cart"></i>
                                    <span className="badge badge-danger badge-shopping-cart">0</span>
                                </Nav.Link>
                            </Nav>
                            <Nav className="pt-3 px-3 w-100">
                                <InputGroup size="sm" className="mb-3">
                                    <FormControl placeholder="Pencarian Produk" className="" />
                                    <InputGroup.Append>
                                        <Button size="sm" variant="secondary" id="inputGroup-sizing-sm">
                                            <i className="fa fa-search"></i>
                                        </Button>
                                    </InputGroup.Append>
                                </InputGroup>
                            </Nav>
                            <Nav>
                                <Nav.Link href="#" className="font-weight-light nav-link-icon">
                                    <i className="fa fa-comments"></i>
                                </Nav.Link>
                                <Nav.Link href="#" className="user-topbar">
                                    <img src="/default-avatar.png" alt="user" className="img-user-topbar" />
                                    <span className="username-topbar">Armisianto</span>
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </div>
                    <div className="w-100 py-2 border-top secondary px-3">
                        <Container>
                            <Nav>
                                <Nav.Link href="#" className="font-weight-light font-12 py-1 pl-0">
                                    Cicilan Tanpa Kartu Kredit
                            </Nav.Link>
                            </Nav>
                        </Container>
                    </div>
                </Navbar>
            </Fragment>
        );
    }

}

export default TopBarComponent;