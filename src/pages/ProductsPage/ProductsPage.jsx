import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import BreadcrumbComponent from '../../components/BreadcrumbComponent/BreadcrumbComponent';
import CardProductComponent from '../../components/CardProductComponent/CardProductComponent';
import TopBarComponent from '../../components/TopBarComponent/TopBarComponent';
import BreadcrumbHelpers from '../../helpers/BreadcrumbHelpers';

const ProductsPage = (props) => {
    let mobile = props.mobile;

    // Breadcrumb
    let urlParams = useParams();
    const breadcumbs = BreadcrumbHelpers.generate_breadcrumb_products(urlParams);

    useEffect(() => {
        document.title = 'eCommerce App - Product';
    })

    return (
        <div className={ mobile ? 'component-mobile' : '' }>
            <TopBarComponent mobile={mobile} />
            <Container className={mobile ? 'mt-0 container-mobile' : 'mt-5'}>
                <div className="row">
                    <div className="col-12 mt-5 pt-4 pb-3">
                        <BreadcrumbComponent lists={breadcumbs} />
                    </div>
                    <div className="col-3">
                        <div className="card">
                            <div className="card-header">
                                Filter
                            </div>
                            <div className="card-body">

                            </div>
                        </div>
                    </div>
                    <div className="col-9">
                        <CardProductComponent mobile={mobile} all={true} />
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default ProductsPage;