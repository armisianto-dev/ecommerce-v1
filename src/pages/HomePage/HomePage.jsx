import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import CardProductComponent from '../../components/CardProductComponent/CardProductComponent';
import CarouselComponent from '../../components/CarouselComponent/CarouselComponent';
import TopBarComponent from '../../components/TopBarComponent/TopBarComponent';

const HomePage = (props) => {
    let mobile = props.mobile;

    useEffect(() => {
        document.title = 'eCommerce App';
    })

    return (
        <div className={mobile ? 'component-mobile' : ''}>
            <TopBarComponent mobile={mobile} />
            <Container className={mobile ? 'mt-0 container-mobile' : 'mt-5'}>
                <div className={mobile ? 'row mx-0' : 'row'}>
                    <div className={mobile ? 'col-12 px-0' : 'col-12 mt-5 py-4'}>
                        <CarouselComponent mobile={mobile} />
                    </div>
                </div>
                <CardProductComponent className="mx-auto" mobile={mobile} title="Penawaran Istimewa" />
                <CardProductComponent className="mx-auto" mobile={mobile} title="Produk Terbaru" />
                <CardProductComponent className="mx-auto" mobile={mobile} title="Produk Populer" />
            </Container>
        </div>
    );
}

export default HomePage;