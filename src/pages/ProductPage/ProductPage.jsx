import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import ReactImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import { useParams } from 'react-router-dom';
import BreadcrumbComponent from '../../components/BreadcrumbComponent/BreadcrumbComponent';
import TopBarComponent from '../../components/TopBarComponent/TopBarComponent';
import BreadcrumbHelpers from '../../helpers/BreadcrumbHelpers';
import './ProductPage.css';

const ProductPage = (props) => {
    let mobile = props.mobile;

    // State
    const stok = 6;
    const [qty, setQty] = useState(1);
    const [isDisableMin, setDisableMin] = useState(true);
    const [isDisableAdd, setDisableAdd] = useState(false);

    const addQty = () => {
        if (qty === stok)
            return;
        setQty(prevQty => prevQty + 1);
    }

    const minQty = () => {
        if (qty === 1)
            return;
        setQty(prevQty => prevQty - 1);
    }

    useEffect(() => {

        document.title = "eCommerce App - HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10";

        if (qty === 1) {
            setDisableMin(true);
            setDisableAdd(false);
        } else if (qty === stok) {
            setDisableMin(false);
            setDisableAdd(true);
        }

    }, [qty]);

    // Breadcrumb
    let urlParams = useParams();
    let breadcrumbs = BreadcrumbHelpers.generate_breadcrumb_product(urlParams);
    let breadcrumbsCategory = [
        { 'title': 'Kategori', 'link': '/products/kategori-1' },
        { 'title': 'Sub Kategori', 'link': '/products/sub-kategori-1' }
    ];

    // Images Product
    const imagesProduct = [
        {
            original: '/products/1.jpg.webp',
            thumbnail: '/products/1.jpg.webp'
        },
        {
            original: '/products/2.jpg.webp',
            thumbnail: '/products/2.jpg.webp'
        },
        {
            original: '/products/3.jpg.webp',
            thumbnail: '/products/3.jpg.webp'
        },
        {
            original: '/products/4.jpg.webp',
            thumbnail: '/products/4.jpg.webp'
        }
    ]

    return (
        <div>
            <TopBarComponent mobile={mobile} />
            <Container className={mobile ? 'mt-0 container-mobile' : 'mt-5'}>
                <div className="row">
                    <div className="col-12 mt-5 pt-4 pb-3">
                        <BreadcrumbComponent lists={breadcrumbs} />
                    </div>
                    <div className="col-12 mt-1">
                        <div className="card border-0 card-product">
                            <div className="card-body">
                                <div className="w-100">
                                    <div className="row">
                                        <div className="col-6 col-md-6">
                                            <ReactImageGallery items={imagesProduct} lazyLoad={true} showPlayButton={false} useBrowserFullscreen={true} thumbnailPosition="left" />
                                        </div>
                                        <div className="col-6 col-md-6">
                                            <BreadcrumbComponent lists={breadcrumbsCategory} category={true} />
                                            <h5 className="card-title">HP Laptop 14s-cf1046TU - Intel Celeron N4205U - 4 GB - 1 TB - W10</h5>
                                            <hr></hr>
                                            <div className="d-flex flex-column mb-3 product-detail-price">
                                                <span className="product-detail-price__original">Rp 4.000.000</span>
                                                <span className="product-detail-price__reduced">Rp 3.589.000</span>
                                            </div>
                                            <div className="d-flex flex-column mb-3 product-detail-stok">
                                                <span className="product-detail-stok__info">Stok tersisa {stok} lagi !</span>
                                                <span className="font-12 mb-1">Masukan jumlah yang anda inginkan</span>
                                                <div className="row mb-2">
                                                    <div className="col-4 col-md-4">
                                                        <div className="input-group input-group-sm">
                                                            <div className="input-group-prepend">
                                                                <button className="btn btn-secondary" type="button" onClick={minQty} disabled={isDisableMin}>-</button>
                                                            </div>
                                                            <input type="text" className="form-control text-center" value={qty} readOnly />
                                                            <div className="input-group-append">
                                                                <button className="btn btn-secondary" type="button" onClick={addQty} disabled={isDisableAdd}>+</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-4 col-md-4">
                                                        <button className="btn btn-sm btn-success btn-block">Tambah Ke Keranjang</button>
                                                    </div>
                                                    <div className="col-4 col-md-4">
                                                        <button className="btn btn-sm btn-primary btn-block">Beli</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default ProductPage;