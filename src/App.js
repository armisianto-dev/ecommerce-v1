import React, { Fragment } from 'react';
import { useMediaQuery } from 'react-responsive';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import HomePage from './pages/HomePage/HomePage';
import ProductPage from './pages/ProductPage/ProductPage';
import ProductsPage from './pages/ProductsPage/ProductsPage';

function App() {
  const isMobileMode = useMediaQuery({
    query: '(max-device-width: 767px)'
  })
  let mobile = isMobileMode ? true : false;
  return (
    <Fragment>
      <Router>
        <Switch>
          <Route exact path="/">
            <HomePage mobile={mobile} />
          </Route>
          <Route path="/products/:cat?/:sub1?/:sub2?">
            <ProductsPage mobile={mobile} />
          </Route>
          <Route path="/product/:brand/:producttitle">
            <ProductPage mobile={mobile} />
          </Route>
        </Switch>
      </Router>
    </Fragment>
  );
}

export default App;
