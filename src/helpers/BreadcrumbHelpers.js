import StringHelpers from "./StringHelpers";

function generate_breadcrumb_products(params) {
    const { cat, sub1, sub2 } = params;
    const breadcumbs = [
        { 'title': 'Home', 'link': '/' },
        { 'title': 'Produk', 'link': '/products' },
    ]

    if (cat) {
        breadcumbs.push({ 'title': StringHelpers.extract_slug(cat), 'link': '/products/' + cat })
    }


    if (sub1) {
        breadcumbs.push({ 'title': StringHelpers.extract_slug(sub1), 'link': '/products/' + cat + '/' + sub1 })
    }


    if (sub2) {
        breadcumbs.push({ 'title': StringHelpers.extract_slug(sub2), 'link': '/products/' + cat + '/' + sub1 + '/' + sub2 })
    }

    return breadcumbs;
}

function generate_breadcrumb_product(params) {
    const { brand, producttitle } = params;
    const breadcumbs = [
        { 'title': 'Home', 'link': '/' },
        { 'title': 'Produk', 'link': '/products' },
    ]

    if (brand) {
        breadcumbs.push({ 'title': StringHelpers.extract_slug(brand), 'link': '/products?brand=' + brand })
    }


    if (producttitle) {
        breadcumbs.push({ 'title': StringHelpers.extract_slug(producttitle), 'link': '/product/' + brand + '/' + producttitle })
    }

    return breadcumbs;
}

const BreadcrumbHelpers = {
    generate_breadcrumb_products,
    generate_breadcrumb_product
}

export default BreadcrumbHelpers;